// allowing the vue template files to access the function
import * as mylib from '../customjs/an-show-panel.js';


var count = 0
var svg
var myg
var small
var start = false
var saveddata = {}
var status = []

function drawone(width,time1,time2,g1c,g2c,name,id, count){
  // ===== background ======
  var g = myg.append('g')
      .attr('class','item')
      .attr('id', 'item'+id)
      .attr("transform", "translate(0,"+ id * 200 +")");

  var rect = g.append('rect')
      .attr('class', 'background')
      .attr('height', 180)
      .attr('width', width - 40)
      .attr('x', 20)
      .attr('y', 10)
      .attr('rx',20)
      .attr('ry',20)
      .style('opacity', 0.2);

  var title = g.append('text')
      .text("item " + (name))
      .attr('x', 40)
      .attr('y', 30);

  // ===== function buttons =====

  // --- clear button ---
  var clearbtn = g.append('g')
      .attr('class', 'buttons')

  var btn = clearbtn.append('rect')
      .attr('class', 'cbtn')
      .attr('id', 'bitem'+id)
      .attr('width', 80)
      .attr('height', 40)
      .attr('rx', 5)
      .attr('ry', 5)
      .attr('x', width - 130)
      .attr('y', 200 - 70)
      .attr('fill', 'red')
      .style('opacity',0.5)
      .on('click', onclick);

  clearbtn.append('text')
      .text('Remove')
      .attr("class", "cbtnt")
      .attr('fill', 'white')
      .attr('x', width - 108)
      .attr('y', 200 - 45);

  // ---- show hidden view ----
  var more = g.append('g')
  .attr('class', 'buttons')

  more.append('circle')
      .attr('class', 'sbtn')
      .attr('r', '20')
      .attr('cx', width - 85)
      .attr('cy', 50)
      .attr('stroke', '#3B5998')
      .attr('stroke-width', 3)
      .attr('fill', 'white')
      .on("click", function() {
        var result = mylib.myfunc.show_hidden_panel(this.id, count, status);
        if(result == 0){
          small = false
        } else {
          small = true
        }
        adjust(myg,width,g1c,g2c);

      });

  var line = d3.line()
      .x(function(d) { return d.x + width - 95; })
      .y(function(d) { return d.y + 35; });

  var points = [[{x:0,y:0}, {x:0,y:30}, {x:26,y:15},{x:0,y:0}]]

  var path = more.selectAll('g')
      .data(points).enter()
      .append('g')
      .attr('class','paths')
  path
      .append('path')
      .attr('d', function(d){ return line(d)})
      .attr('class','path')
      .attr('id','s'+id)
      .attr('fill', '#3B5998')
      .style('opacity',0.8)
      .on("click", function() {
        var result = mylib.myfunc.show_hidden_panel(this.id, count, status);
        if(result == 0){
          small = false
        } else {
          small = true
        }
        adjust(myg,width,g1c,g2c);

      });


  // ===== main view ======
  // ---- gradient color -----
  var main = g.append('g')
      .attr('class','mains')

  var defs = svg.append('defs')
  var linearGradient1 = defs.append('linearGradient')
      .attr('id', 'linear-gradient1');

  // horizontal gradient
  linearGradient1
      .attr('x1','0%')
      .attr('y1','0%')
      .attr('x2','100%')
      .attr('y2','0%')
  linearGradient1.append('stop')
      .attr('offset', '0%')
      .attr('stop-color', '#ffA500')
  linearGradient1.append('stop')
      .attr('offset', '100%')
      .attr('stop-color', '#0000ff')


  var linearGradient2 = defs.append('linearGradient')
      .attr('id', 'linear-gradient2');

  // horizontal gradient
  linearGradient2
      .attr('x1','0%')
      .attr('y1','0%')
      .attr('x2','100%')
      .attr('y2','0%')
  linearGradient2.append('stop')
      .attr('offset', '0%')
      .attr('stop-color', '#0000ff')
  linearGradient2.append('stop')
      .attr('offset', '100%')
      .attr('stop-color', '#ffA500')


  // ---- prepare ----

  var x = d3.scaleTime().range([0, width * 0.5]);
  var y1 = d3.scaleLinear().range([60,0]);
  var y2 = d3.scaleLinear().range([0,60]);


  var valueline1 = d3.line()
      .x(function(d){ return x(d.date);})
      .y(function(d){ return y1(d.data);})
      .curve(d3.curveCardinal)
  var valueline2 = d3.line()
      .x(function(d){ return x(d.date);})
      .y(function(d){ return y2(d.data);})
      .curve(d3.curveCardinal)
  var area1 = d3.area()
      .x(function(d){ return x(d.date);})
      .y0(60)
      .y1(function(d){ return y1(d.data);})
      .curve(d3.curveCardinal)
  var area2 = d3.area()
      .x(function(d){ return x(d.date);})
      .y0(0)
      .y1(function(d){ return y2(d.data);})
      .curve(d3.curveCardinal)

  // ---- data -----
  /*
  time1.forEach(function(d) {
      d.date = parseTime(d.date);
      d.data = +d.data;
  });
  time2.forEach(function(d) {
      d.date = parseTime(d.date);
      d.data = +d.data;
  });
  */

  x.domain(d3.extent(time1, function(d) { return d.date; }));
  var both = d3.max([d3.max(time1, function(d) { return d.data; }),d3.max(time2, function(d) { return d.data; })])
  y1.domain([0, both]);
  y2.domain([0, both]);

  // ---- g1 area -----
  var tmp = main.append('path')
      .data([time1])
      .attr('class','up area')
      .attr("transform", "translate(" + width * 0.25 + ',' + 40 + ")")
      .attr('d', area1)
      .attr('fill', 'url(#linear-gradient1)')
      .attr('opacity', 0.5)
  // ---- g1 line -----
  main.append('path')
      .data([time1])
      .attr('class', 'up line')
      .attr("transform", "translate(" + width * 0.25 + ',' + 40 + ")")
      .attr('d',valueline1)

  // ---- g2 area -----
  main.append('path')
      .data([time2])
      .attr('class','down area')
      .attr("transform", "translate(" + width * 0.25 + ',' + 100 + ")")
      .attr('d', area2)
      .attr('fill', 'url(#linear-gradient2)')
      .attr('opacity', 0.5)

  // ---- g2 line -----
  main.append('path')
      .data([time2])
      .attr('class', 'down line')
      .attr("transform", "translate(" + width * 0.25 + ',' + 100 + ")")
      .attr('d',valueline2)

  // --- axis -----
  main.append('g')
      .attr("class", "mainaxis")
      .attr("transform", "translate(" + width * 0.25 + ',' +  100 + ")")
      .call(d3.axisBottom(x)
          .tickFormat(d3.timeFormat("%y-%m")))

  main.append('g')
      .attr("class", "upaxis")
      .attr("transform", "translate(" + width * 0.5  + ',' +  40 + ")")
      .call(d3.axisLeft(y1).ticks(3))

  main.append('g')
      .attr("class", "downaxis")
      .attr("transform", "translate(" + width * 0.5 + ',' +  100 + ")")
      .call(d3.axisRight(y2).ticks(3))

  // ---- g1 node -----

  main.append('circle')
      .attr('class','group1c')
      .attr('r', g1c)
      .attr('cx', width * 0.1)
      .attr('cy', 80)
      .attr('fill', 'orange')
      .style('opacity', 0.5)

  main.append('text')
      .attr('class','group1t')
      .text('Group 1')
      .attr('x', width * 0.1 + d3.max([g1c,g2c]) + 20)
      .attr('y', 80)

  // ---- g2 node -----

  main.append('circle')
      .attr('class','group2c')
      .attr('r', g2c)
      .attr('cx', width * 0.1)
      .attr('cy', 120)
      .attr('fill', 'blue')
      .style('opacity', 0.5)

  main.append('text')
      .attr('class','group2t')
      .text('Group 2')
      .attr('x', width * 0.1 + d3.max([g1c,g2c]) + 20)
      .attr('y', 120)

}

function addsvg(){
  if (start === true){
    var width = $("#an-right-panel").width();
    var height = 200 * 5;
        svg = d3.select("#an-right-panel").append("svg")
            .attr('id','myright')
            .attr("width", width)
            .attr("height", height);
        myg = svg.append('g').attr('class','myg')
        status = []
        for(var i = 0; i < count; i++){
            status.push(0)
        }
        for(var i = 0; i < count; i++){
            var tmp = saveddata[i]
            drawone(width, tmp.time1, tmp.time2, tmp.g1c, tmp.g2c,tmp.name, i, count);
        }
      }

}

function onclick(d){
  var rm = this.id.substring(1)
  d3.select('#'+rm).remove()
  var numofitem = count
  var current = parseInt(this.id.substring(5)) + 1
  for (var i = current; i < numofitem; i++){
    var tag = "item" + i
    var adjust = d3.select('.myg').select('#'+tag)
    adjust.id = 'item' + (i-1)
    adjust
        .attr('id','item' + (i - 1))
        .attr("transform", "translate(0,"+ (i - 1) * 200 +")")
    var adjc = d3.select('#bitem' + i)
    adjc.attr('id','bitem' + (i - 1))
    saveddata[i-1] = saveddata[i]
  }
  delete saveddata[count - 1]
  count = count - 1
}

function getdata(){
    var data = []
    var dates = ['31-Dec-11', '10-Mar-12', '10-Jun-12','20-Sep-12','10-Dec-12',
                '10-Mar-13', '10-Jun-13','20-Sep-13','10-Dec-13',
                '10-Mar-14', '10-Jun-14','20-Sep-14','10-Dec-14',
                '10-Mar-15', '10-Jun-15','20-Sep-15','10-Dec-15']
    for(var i = 0; i < dates.length; i++){
      data.push({'date':dates[i],'data':Math.floor(Math.random() * 95) + 5})
    }
    return data
}

function adjust(g, width, g1, g2){
  if (small === false){
    var items = g.selectAll('.item').each(function(d){
      smallitem(d3.select(this), width, g1, g2, small)
    })
    //small = true
  } else {
    var items = g.selectAll('.item').each(function(d){
      smallitem(d3.select(this), width, g1, g2, small)
    })
    //small = false
  }
}

function smallitem(g, width, g1, g2, type){
  if (type === false){
      // ====== button =======
      g.selectAll('.buttons').select('.cbtn')
          .attr('x', width*2/3 - 80)
          .attr('y',200 - 45)
          .attr('width',40)
          .attr('height',20)

      g.selectAll('.buttons').select('.cbtnt')
          .attr('x', width*2/3 - 75)
          .attr('y',200 - 30)
      g.selectAll('.buttons').select('.sbtn')
          .attr('cx', width*2/3 - 56)

      g.selectAll('.buttons').select('.paths')
          .attr("transform", "translate(" + (-width/3 + 28) + ',' + 0 + ")")

      // ====== background ======
      g.selectAll('.background')
          .attr('width', width*2/3 - 50)

      // ====== main view ======
      g.selectAll('.mains').selectAll('.up')
          .attr("transform", "translate(" + width * 0.08 + ',' + 40 + ")")
      g.selectAll('.mains').selectAll('.down')
          .attr("transform", "translate(" + width * 0.08 + ',' + 100 + ")")
      g.selectAll('.mains').selectAll('.upaxis')
          .attr("transform", "translate(" + width * 0.33  + ',' +  40 + ")")
      g.selectAll('.mains').selectAll('.downaxis')
          .attr("transform", "translate(" + width * 0.33  + ',' +  100 + ")")
      g.selectAll('.mains').selectAll('.mainaxis')
          .attr("transform", "translate(" + width * 0.08 + ',' +  100 + ")")
      g.selectAll('.mains').selectAll('.group1c')
          .attr('cx', width * 0.05)
          .attr('cy', 70)
      g.selectAll('.mains').selectAll('.group1t')
          .attr('x', width * 0.03)
          .attr('y', 70 + 20 + 15)
      g.selectAll('.mains').selectAll('.group2c')
          .attr('cx', width * 0.05)
          .attr('cy',130)
      g.selectAll('.mains').selectAll('.group2t')
          .attr('x', width * 0.03)
          .attr('y',130 + 20 + 15)

  } else {
      // ====== button =======
      g.selectAll('.buttons').select('.cbtn')
          .attr('x', width - 130)
          .attr('width',80)
          .attr('height',40)
          .attr('y',200 - 70)
      g.selectAll('.buttons').select('.cbtnt')
          .attr('x', width - 108)
          .attr('y',200 - 45)
      g.selectAll('.buttons').select('.sbtn')
          .attr('cx', width - 85)
      g.selectAll('.buttons').select('.paths')
          .attr("transform", "translate(" + 0 + ',' + 0 + ")")
      // ====== background ======
      g.selectAll('.background')
          .attr('width', width - 50)

      // ====== main view ======
      g.selectAll('.mains').selectAll('.up')
          .attr("transform", "translate(" + width * 0.25 + ',' + 40 + ")")
      g.selectAll('.mains').selectAll('.down')
          .attr("transform", "translate(" + width * 0.25 + ',' + 100 + ")")
      g.selectAll('.mains').selectAll('.upaxis')
          .attr("transform", "translate(" + width * 0.5  + ',' +  40 + ")")
      g.selectAll('.mains').selectAll('.downaxis')
          .attr("transform", "translate(" + width * 0.5  + ',' +  100 + ")")
      g.selectAll('.mains').selectAll('.mainaxis')
          .attr("transform", "translate(" + width * 0.25 + ',' +  100 + ")")
      g.selectAll('.mains').selectAll('.group1c')
          .attr('cx', width * 0.1)
          .attr('cy', 80)
      g.selectAll('.mains').selectAll('.group1t')
          .attr('x', width * 0.1 + d3.max([g1,g2]) + 20)
          .attr('y', 80)
      g.selectAll('.mains').selectAll('.group2c')
          .attr('cx', width * 0.1)
          .attr('cy', 120)
      g.selectAll('.mains').selectAll('.group2t')
          .attr('x', width * 0.1 + d3.max([g1,g2]) + 20)
          .attr('y', 120)
  }
}

function myload(timedata){
  if (count > 4){
    alert("Please remove before adding new item");
  } else {
      // ===== data ======
      //var time1 = timedata[0]
      var time1 = getdata()
      var time2 = getdata()
      var g1c = Math.floor(Math.random() * 15) + 5
      var g2c = Math.floor(Math.random() * 15) + 5
      status.push(0)
      //var g1c = 20
      //var g2c = 20
      //var parseTime = d3.timeParse("%Y-%m-%dT%H:%M:00Z");
      var parseTime = d3.timeParse("%d-%b-%y");

      saveddata[count] = {'time1':time1, 'time2':time2, 'g1c':g1c, 'g2c':g2c, 'name':count + 1}


      var width = $("#an-right-panel").width();
      var height = 200 * 5;
      if(start === false){
          svg = d3.select("#an-right-panel").append("svg")
              .attr('id','myright')
              .attr("width", width)
              .attr("height", height);
          myg = svg.append('g').attr('class','myg')
          start = true
      }

      // ===== background ======
      var g = myg.append('g')
          .attr('class','item')
          .attr('id', 'item'+count)
          .attr("transform", "translate(0,"+ count * 200 +")");

      var rect = g.append('rect')
          .attr('class', 'background')
          .attr('height', 180)
          .attr('width', width - 40)
          .attr('x', 20)
          .attr('y', 10)
          .attr('rx',20)
          .attr('ry',20)
          .style('opacity', 0.2);

      var title = g.append('text')
          .text("item " + (count+1))
          .attr('x', 40)
          .attr('y', 30);

      // ===== function buttons =====

      // --- clear button ---
      var clearbtn = g.append('g')
          .attr('class', 'buttons')

      var btn = clearbtn.append('rect')
          .attr('class', 'cbtn')
          .attr('id', 'bitem'+count)
          .attr('width', 80)
          .attr('height', 40)
          .attr('rx', 5)
          .attr('ry', 5)
          .attr('x', width - 130)
          .attr('y', 200 - 70)
          .attr('fill', 'red')
          .style('opacity',0.5)
          .on('click', onclick);

      clearbtn.append('text')
          .text('Remove')
          .attr("class", "cbtnt")
          .attr('fill', 'white')
          .attr('x', width - 108)
          .attr('y', 200 - 45);

      // ---- show hidden view ----
      var more = g.append('g')
      .attr('class', 'buttons')

      more.append('circle')
          .attr('class', 'sbtn')
          .attr('r', '20')
          .attr('cx', width - 85)
          .attr('cy', 50)
          .attr('stroke', '#3B5998')
          .attr('stroke-width', 3)
          .attr('fill', 'white')
          .on("click", function() {
              var result = mylib.myfunc.show_hidden_panel(this.id, count, status);
              if(result == 0){
                small = false
              } else {
                small = true
              }
              adjust(myg,width,g1c,g2c);
          });

      var line = d3.line()
          .x(function(d) { return d.x + width - 95; })
          .y(function(d) { return d.y + 35; });

      var points = [[{x:0,y:0}, {x:0,y:30}, {x:26,y:15},{x:0,y:0}]]

      var path = more.selectAll('g')
          .data(points).enter()
          .append('g')
          .attr('class','paths')
      path
          .append('path')
          .attr('d', function(d){ return line(d)})
          .attr('class','path')
          .attr('id','s'+count)
          .attr('fill', '#3B5998')
          .style('opacity',0.8)
          .on("click", function() {
              var result = mylib.myfunc.show_hidden_panel(this.id, count, status);
              if(result == 0){
                small = false
              } else {
                small = true
              }
              adjust(myg,width,g1c,g2c);
          });


      // ===== main view ======
      // ---- gradient color -----
      var main = g.append('g')
          .attr('class','mains')

      var defs = svg.append('defs')
      var linearGradient1 = defs.append('linearGradient')
          .attr('id', 'linear-gradient1');

      // horizontal gradient
      linearGradient1
          .attr('x1','0%')
          .attr('y1','0%')
          .attr('x2','100%')
          .attr('y2','0%')
      linearGradient1.append('stop')
          .attr('offset', '0%')
          .attr('stop-color', '#ffA500')
      linearGradient1.append('stop')
          .attr('offset', '100%')
          .attr('stop-color', '#0000ff')


      var linearGradient2 = defs.append('linearGradient')
          .attr('id', 'linear-gradient2');

      // horizontal gradient
      linearGradient2
          .attr('x1','0%')
          .attr('y1','0%')
          .attr('x2','100%')
          .attr('y2','0%')
      linearGradient2.append('stop')
          .attr('offset', '0%')
          .attr('stop-color', '#0000ff')
      linearGradient2.append('stop')
          .attr('offset', '100%')
          .attr('stop-color', '#ffA500')


      // ---- prepare ----

      var x = d3.scaleTime().range([0, width * 0.5]);
      var y1 = d3.scaleLinear().range([60,0]);
      var y2 = d3.scaleLinear().range([0,60]);

      var valueline1 = d3.line()
          .x(function(d){ return x(d.date);})
          .y(function(d){ return y1(d.data);})
          .curve(d3.curveCardinal)
      var valueline2 = d3.line()
          .x(function(d){ return x(d.date);})
          .y(function(d){ return y2(d.data);})
          .curve(d3.curveCardinal)
      var area1 = d3.area()
          .x(function(d){ return x(d.date);})
          .y0(60)
          .y1(function(d){ return y1(d.data);})
          .curve(d3.curveCardinal)
      var area2 = d3.area()
          .x(function(d){ return x(d.date);})
          .y0(0)
          .y1(function(d){ return y2(d.data);})
          .curve(d3.curveCardinal)

      // ---- data -----

      time1.forEach(function(d) {
          d.date = parseTime(d.date);
          d.data = +d.data;
      });
      time2.forEach(function(d) {
          d.date = parseTime(d.date);
          d.data = +d.data;
      });


      x.domain(d3.extent(time1, function(d) { return d.date; }));
      var both = d3.max([d3.max(time1, function(d) { return d.data; }),d3.max(time2, function(d) { return d.data; })])
      y1.domain([0, both]);
      y2.domain([0, both]);

      // ---- g1 area -----
      var tmp = main.append('path')
          .data([time1])
          .attr('class','up area')
          .attr("transform", "translate(" + width * 0.25 + ',' + 40 + ")")
          .attr('d', area1)
          .attr('fill', 'url(#linear-gradient1)')
          .attr('opacity', 0.5)
      // ---- g1 line -----
      main.append('path')
          .data([time1])
          .attr('class', 'up line')
          .attr("transform", "translate(" + width * 0.25 + ',' + 40 + ")")
          .attr('d',valueline1)

      // ---- g2 area -----
      main.append('path')
          .data([time2])
          .attr('class','down area')
          .attr("transform", "translate(" + width * 0.25 + ',' + 100 + ")")
          .attr('d', area2)
          .attr('fill', 'url(#linear-gradient2)')
          .attr('opacity', 0.5)

      // ---- g2 line -----
      main.append('path')
          .data([time2])
          .attr('class', 'down line')
          .attr("transform", "translate(" + width * 0.25 + ',' + 100 + ")")
          .attr('d',valueline2)

      // --- axis -----
      main.append('g')
          .attr("class", "mainaxis")
          .attr("transform", "translate(" + width * 0.25 + ',' +  100 + ")")
          .call(d3.axisBottom(x)
              .tickFormat(d3.timeFormat("%y-%m")))

      main.append('g')
          .attr("class", "upaxis")
          .attr("transform", "translate(" + width * 0.5  + ',' +  40 + ")")
          .call(d3.axisLeft(y1).ticks(3))

      main.append('g')
          .attr("class", "downaxis")
          .attr("transform", "translate(" + width * 0.5 + ',' +  100 + ")")
          .call(d3.axisRight(y2).ticks(3))

      // ---- g1 node -----

      main.append('circle')
          .attr('class','group1c')
          .attr('r', g1c)
          .attr('cx', width * 0.1)
          .attr('cy', 80)
          .attr('fill', 'orange')
          .style('opacity', 0.5)

      main.append('text')
          .attr('class','group1t')
          .text('Group 1')
          .attr('x', width * 0.1 + d3.max([g1c,g2c]) + 20)
          .attr('y', 80)

      // ---- g2 node -----

      main.append('circle')
          .attr('class','group2c')
          .attr('r', g2c)
          .attr('cx', width * 0.1)
          .attr('cy', 120)
          .attr('fill', 'blue')
          .style('opacity', 0.5)

      main.append('text')
          .attr('class','group2t')
          .text('Group 2')
          .attr('x', width * 0.1 + d3.max([g1c,g2c]) + 20)
          .attr('y', 120)

      count = count + 1;
  }

}

exports.myfunc = {

    // "reloc_rp": function ()
    // {
    //     rightp_loc();
    // }

    "add_item": function (timedata)
    {
        console.log(timedata)
        myload(timedata)
    },
    "jsredraw": function () {
        // addsvg()
        $(document).ready(function()
        {
          addsvg();
        })
    }

}
