
var open = 0;

// allowing the vue template files to access the function
exports.myfunc = {

    // "reloc_rp": function ()
    // {
    //     rightp_loc();
    // }
    "show_hidden_panel": function (id, count, status)
    {

        var right = document.getElementById( 'an-right-panel' );
        var hidden = document.getElementById( 'an-hidden-panel' );
        console.log(status)

        if( !status[id[1]] )
        {
          var other = -1
          for(var i = 0; i < count; i++){
            if(i == id[1]){
              continue
            }
            if(status[i] == 1){
              other = i
              status[i] = 0
            }
          }
          if(other == -1){
            status[id[1]] = 1;
            right.classList = "col-6";
            hidden.classList = "col-3";
            // show the hidden panel
            setTimeout( function()
            {
                hidden.style.display = "inline";
            }, 500);
            return 0
          } else {
            status[id[1]] = 1
            return 0
          }
        }
        else
        {
            var other = -1
            for(var i = 0; i < count; i++){
              if(i == id[1]){
                continue
              }
              if(status[i] == 1){
                other = i
                status[i] = 0
              }
            }
            if(other == -1){
              status[id[1]] = 0
              right.classList = "col-9";
              hidden.classList = "col";
              // hide the hidden panel
              hidden.style.display = "none";
              return 1
            } else {
              return 0
            }
        }
    }
}
