
// Force-Directed Graph
// Ref. https://bl.ocks.org/mbostock/4062045

var offset = 10;

exports.myfunc = {
    draw: function( graph, cate, rela, spliter )
    {
        console.log( rela );
        let margin = { top: 10, right: 10, bottom: 20, left: 10 };

        var div = d3.select( '#cl-graph-container' );

        var width = +document.getElementById( 'cl-graph-container' ).clientWidth,
            height = +document.getElementById( 'cl-graph-container' ).clientHeight;

        var svg = div.append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom);

        var color = d3.scaleOrdinal(d3.schemeCategory20);
        // var colors = [ "#178BCA", "#FF4444", "#3CA55C" ];

        var simulation = d3.forceSimulation()
            // set the distance between nodes
            // .force( "link", d3.forceLink().id(function(d) { return d.name; }).distance( 200 ) )
            .force( "link", d3.forceLink().id(function(d) { return d.name; }) )
            .force( "charge", d3.forceManyBody().strength(-5000) )
            // .force( "charge", d3.forceManyBody() )
            .force( "center", d3.forceCenter(width / 2, height / 2) )
            .force( 'collision', d3.forceCollide().radius( function( d )
            {
                return d.radius
            }));

        // Per-type markers, as they don't inherit styles.
        svg.append("svg:defs").selectAll("marker")
            .data([ "ego", "alter" ])
            .enter().append("svg:marker")
            .attr("id", String)
            .attr("viewBox", "0 -5 10 10")
            .attr("refX", 15)
            .attr("refY", -1.5)
            .attr("markerWidth", 6)
            .attr("markerHeight", 6)
            .attr("orient", "auto")
            .append("svg:path")
            .attr("d", "M0,-5L10,0L0,5");

        var link = svg.append("g")
            .attr("class", "links")
            .selectAll("line")
            .data( graph.links )
            .enter().append("path")
            // .attr("stroke-width", function(d) { return Math.sqrt(d.value); })
            .attr("stroke-width", function(d) { return Math.log(d.value); })
            // .attr("stroke-width", function(d) { return d.value; })
            .attr("class", function(d) { return "fd-link " + d.type; });
            // .attr("marker-end", function(d) { return "url(#" + d.type + ")"; });

            var node = svg.append("g")
            .attr( "class", "nodes" )
            .selectAll( "circle" )
            .data( graph.nodes )
            .enter().append("circle")
            // .each( collide( .5 ) )
            // .attr("r", function( d )
            // {
            //     // return Math.sqrt(d.value);
            //     // return d.value * 2;
            //     // return 8;
            //     return d.value;
            // })
            .attr("r", function( d )
            {
                // return Math.log10(d.value);
                return Math.sqrt(d.value) * 3;
                // return d.value * 2;
                // return 8;
                // return d.value;
            })
            // .attr("fill", function(d) { return color(d.value); })
            .call( d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended))
                .on( "click", function( d ){}
            );

        // the title of the nodes --------------------------------------------------
        var text = svg.append("svg:g").selectAll("g")
            .data( graph.nodes )
            .enter().append("svg:g");

        // A copy of the text with a thick white stroke for legibility.
        text.append("svg:text")
            .attr("x", 8)
            .attr("y", ".31em")
            .attr("class", "shadow")
            .text(function(d) { return d.name; });

        text.append("svg:text")
            .attr("x", 8)
            .attr("y", ".31em")
            .text(function(d) { return d.name; });
        // -------------------------------------------------- the title of the nodes

        simulation.nodes(graph.nodes)
            .on("tick", ticked);

        simulation.force("link")
            .links(graph.links);

        // Use elliptical arc path segments to doubly-encode directionality.
        function ticked()
        {
            // link.attr("x1", function(d) { return d.source.x;})
            //     .attr("y1", function(d) { return d.source.y;})
            //     .attr("x2", function(d) { return d.target.x;})
            //     .attr("y2", function(d) { return d.target.y;});
            link.attr("d", function( d )
            {    
                // var dx = d.target.x - d.source.x,
                //     dy = d.target.y - d.source.y,
                //     dr = 150 / d.linknum;
                // return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;

                // var r = rela[ d.target.name + spliter + d.source.name ];
                // if( r != undefined )
                //     if( r[ "type" ] == 'alter' )
                //     {

                //     }
                //     else
                //     {

                //     }
                
                
                // Ref. https://stackoverflow.com/questions/13608186/trying-to-plot-coordinates-around-the-edge-of-a-circle
                // get the new location of the line
                var a = d.target.x - d.source.x;
                var b = d.target.y - d.source.y;
                var c = Math.sqrt( Math.pow( a, 2 ) + Math.pow( b, 2 ) );

                // var r1 = d.source.value;
                var r1 = Math.sqrt( d.source.value ) * 1.5;
                // var r1 = Math.log10( d.source.value );

                var cos1 = a / c;
                var sin1 = b / c;
                var nx1 = d.source.x + r1 * cos1;
                var ny1 = d.source.y + r1 * sin1;

                a = d.source.x - d.target.x;
                b = d.source.y - d.target.y;
                c = Math.sqrt( Math.pow( a, 2 ) + Math.pow( b, 2 ) );
                
                // var r2 = d.target.value;
                var r2 = Math.sqrt( d.target.value ) * 1.5;
                // var r2 = Math.log10( d.target.value );

                var cos2 = a / c;
                var sin2 = b / c;
                var nx2 = d.target.x + r2 * cos2;
                var ny2 = d.target.y + r2 * sin2;

                // console.log( rela[ d.target.name + spliter + d.source.name ] );
                var r = rela[ d.target.name + spliter + d.source.name ];
                if( r != undefined )
                    if( r[ "type" ] == 'alter' )
                        return "M" + (nx1) + "," + (ny1 + offset) +
                               " L" + (nx2) + "," + (ny2 + offset);
                    else
                        return "M" + (nx1) + "," + (ny1 - offset) +
                               " L" + (nx2) + "," + (ny2 - offset);


                // return "M" + (d.source.x) + "," + (d.source.y + (d.linknum*4)) +
                //        ",L " + d.target.x + "," + d.target.y + (d.linknum*4);
                // console.log( [d.target, d.source] );

                // return "M" + (nx1) + "," + (ny1) +
                //        ",L " + (nx2) + "," + (ny2);
            });
            
            node.attr( "transform", function( d )
            {
                return "translate(" + d.x + "," + d.y + ")";
            });
            
            text.attr("transform", function(d) {
                return "translate(" + (d.x - 10) + "," + (d.y + 10) + ")";
            });
        }

        function dragstarted(d)
        {
            if (!d3.event.active) simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged(d)
        {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragended(d)
        {
            if (!d3.event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }

        // var maxRadius = 12, padding = 1.5;
        // // Resolves collisions between d and all other circles.
        // function collide( alpha )
        // {
        //     var quadtree = d3.quadtree( graph.nodes );
        //     return function( d ) {
        //         var r = d.value + maxRadius + padding,
        //             nx1 = d.x - r,
        //             nx2 = d.x + r,
        //             ny1 = d.y - r,
        //             ny2 = d.y + r;
        //         quadtree.visit(function(quad, x1, y1, x2, y2) {
        //             if (quad.point && (quad.point !== d)) {
        //             var x = d.x - quad.point.x,
        //                 y = d.y - quad.point.y,
        //                 l = Math.sqrt(x * x + y * y),
        //                 r = d.radius + quad.point.radius + padding;
        //             if (l < r) {
        //                 l = (l - r) / l * alpha;
        //                 d.x -= x *= l;
        //                 d.y -= y *= l;
        //                 quad.point.x += x;
        //                 quad.point.y += y;
        //             }
        //             }
        //             return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
        //         });
        //     };
        // }
    }
};