/* TimeSlider Creation
 *   Ref. https://www.jqueryscript.net/time-clock/Custom-Schedule-Timeline-Time-Slider-Plugin-TimeSlider.html
 */
var current_time = (new Date()).getTime() + ((new Date()).getTimezoneOffset() * 60 * 1000 * -1);


function create_timeslider( start, end )
{
    console.log( 'Hippo' );
    console.log( start );
    console.log( end );
    
    $( '#cl-slider-zoom' ).slider(
    {
        min: 1,
        max: 1000,
        // value: 24,
        step: 0.5,
        slide: function( event, ui )
        {
            console.log( ui.value );
            $('#cl-slider').TimeSlider( {days_per_ruler: ui.value} );
        }
    });

    $( '#cl-slider' ).TimeSlider(
    {
        // start_timestamp: current_time - 3600 * 12 * 1000,
        start_timestamp: start,
        end_timestamp: end,
        init_cells: [
            {
                '_id': 'c1',
                // 'start': (current_time - (3600 * 5.4 * 1000) + 1234),
                // 'stop': current_time - 3600 * 3.2 * 1000,
                'start': start,
                'stop': end,
                'style': {
                    'background-color': '#76C4FF'
                }
            },
            // {
            //     '_id': 'c2',
            //     'start': (current_time - (3600 * 2.1 * 1000))
            // }
    ]});
}

// allowing the vue template files to access the function
exports.myfunc = {
    // c stands for create
    "c_timeslider": function ( start, end )
    {
        create_timeslider( start, end );
    }
}