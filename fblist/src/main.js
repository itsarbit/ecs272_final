// --------------------- Stylesheet --------------------- //
import scss from './stylesheets/app.scss'

// ----------------------- Pages ------------------------ //
import App from './components/App.vue'
import Cluster from './components/Cluster.vue'
import Analysis from './components/Analysis.vue'
import About from './components/About.vue'
// import Home from './components/Home.vue'

// -------------------- Library link -------------------- //
import Vue from 'vue'
import Resource from 'vue-resource'
import Router from 'vue-router'
/* the Bootstrap-Vue library
 *   Ref. https://bootstrap-vue.js.org/docs/
 */
import BootstrapVue from 'bootstrap-vue'

// link Bootstrap, Bootstrap-Vue stylesheets and JS libs.
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap-vue/dist/bootstrap-vue.js'

// Install plugins
Vue.use( Router );
Vue.use( Resource );
Vue.use( BootstrapVue );

// route config
let routes = [
  // {
  //   path: '/home',
  //   name: 'home',
  //   component: Home
  // },
  {
    path: '/cluster',
    name: 'cluster',
    component: Cluster
  },
  {
    path: '/analysis',
    name: 'analysis',
    component: Analysis
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  // { path: '*', redirect: '/home' }
  { path: '*', redirect: '/cluster' }
]

// Set up a new router
let router = new Router({
  routes: routes
})

// Start up our app
new Vue({
  router: router,
  render: h => h(App)
}).$mount('#app')

/* Vue.js Tutorials
 *
 * 1. https://coursetro.com/posts/code/71/Build-your-First-Vue.js-App-%7C-Vue.js-2.0-Tutorial
 * 2. https://medium.com/codingthesmartway-com-blog/vue-js-2-quickstart-tutorial-2017-246195cfbdd2
 * 
 */
