package main

import (
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

// 169.237.4.227

func main() {
	backend, err := url.Parse("http://ip:8081")
	if err != nil {
		panic(err)
	}
	proxyBackend := httputil.NewSingleHostReverseProxy(backend)

	frontend, err := url.Parse("http://localhost:8082")
	if err != nil {
		panic(err)
	}
	proxyFrontend := httputil.NewSingleHostReverseProxy(frontend)

	http.Handle("/api/", proxyBackend)
	http.Handle("/web/", http.StripPrefix("/web/", proxyFrontend))
	// TODO: modify fblist/src/customjs/config.js port to 8084
	log.Fatal(http.ListenAndServe(":8084", nil))
}
